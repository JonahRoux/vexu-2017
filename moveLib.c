
// Define useful constants (these constants are useful when used)
static int axleTurn = 360; // defines a turn for the axle encoder
static float wheelTurn = 192; // defines encoder counts for one turn of high speed motor
static float dimAvg = 14.0; // average of the dimensions of the robot
static signed char halfPower = 63; // defines half speed for any moto
static signed char maxPower = 127; // full power for motors
static signed char threshold = 15; // defines the controller deadzone threshold for filtering

bool slowMo = false;		// allows more precise control of drive train

#define gyroIn in8

// this function actually powers the motors
void driveTrain(signed char left, signed char right)
{
	// apply power to the motors
	motor[F_LEFT] = left / (1 + slowMo); // cut the power in half when slow mode enabled
	motor[B_LEFT] = left / (1 + slowMo);
	motor[F_RIGHT] = right / (1 + slowMo);
	motor[B_RIGHT] = right / (1 + slowMo);
}

/* This function will move the robot in a way that is
	straight. It will move straight and only straight. Enter
	an argument for 'dist' that is positive or negative. Negative
	values will drive the robot backwards.*/
	// [distance]
void driveStraight(float totalLength)
{
	resetMotorEncoder(B_LEFT);
	resetMotorEncoder(B_RIGHT);
	float distLeft = 0.0; // keeps track of the left encoder's traveled distance
	float distRight = 0.0;
	float unitCount = 11.0 / wheelTurn;

	int sign = sgn(totalLength);
	//determine distance per encoder count
	time1[T1] = 0; // keep track of time
	int prevClick[2] = {0,0};
	int nextClick[2] = {0,0};
	float vels[2] = {0.0,0.0}; // velocity of left and right motors

	while((fabs(totalLength) - (0.5 * (distLeft + distRight))) > 0.25)
	{
		distLeft = abs(nMotorEncoder[B_LEFT]) * unitCount;
		distRight = abs(nMotorEncoder[B_RIGHT]) * unitCount;
		int powers[2] = {1,1};
		//int timetoDrive = a * totalLength;
		if(time1[T1] == 100) // every tenth second measure velocity
		{
			powers[0] = 1;
			powers[1] = 1;
			nextClick[0] = nMotorEncoder[B_LEFT];
			nextClick[1] = nMotorEncoder[B_RIGHT];
			vels[0] = abs(nextClick[0] - prevClick[0]);
			prevClick[0] = nextClick[0];
			vels[1] = abs(nextClick[1] - prevClick[1]);
			prevClick[1] = nextClick[1];
			float velMod = 0.9;
			time1[T1] = 0; // reset the timer
			if(vels[0] > vels[1])
			{
				SensorValue[redLight] = 1;
				SensorValue[greenLight] = 0;
				powers[0] = velMod;
			}
			else if(vels[1] > vels[0])
			{
				SensorValue[redLight] = 0;
				SensorValue[greenLight] = 1;
				powers[1] = velMod;
			}
			else
			{
				SensorValue[redLight] = 0;
				SensorValue[greenLight] = 0;
			}
		}
		driveTrain(powers[0] * 50 * sign , powers[1] * 50 * sign); // apply power
	}
}

void turn(int dir)
{
	/* using a degree value for argument, positive indicates a clockwise turn,
	negative denotes a counterclockwise turn. Requires analog gyro */
	int sign = sgn(dir);
	int degrees10 = dir * 10;
	int originalPos = abs(SensorValue[gyroIn]);
	while(abs(SensorValue[gyroIn]) - originalPos < abs(degrees10))
	{
		driveTrain(-sign * 25, sign * 25);
	}
}
/*void turn(float dir)
{
	const float root2 = 1.4142136; // good number
	float unitCount = 11.0 / wheelTurn; // distance traveled per encoder count
	float totalLength = ((2.0 * 3.1415927 * dimAvg / root2) * (fabs(dir) / 360.0)); // arc length of turn
	resetMotorEncoder(B_LEFT); // always start fresh
	resetMotorEncoder(B_RIGHT);

	float distLeft = 0.0; // keeps track of the left encoder's traveled distance
	float distRight = 0.0;

	time1[T1] = 0; // keep track of time
	int prevClick[2] = {0,0};
	int nextClick[2] = {0,0};
	float vels[2] = {0.0,0.0}; // velocity of left and right motors

	int sign = sgn(dir); // multiplier for turn direction (default counterclockwise)

	while((totalLength - (0.5 * (distLeft + distRight))) > 0.25)
	{ // robot will turn until the average of the encoder counts is within some threshold
		//update distances
		distLeft = (abs(nMotorEncoder[B_LEFT]) * unitCount);
		distRight = (abs(nMotorEncoder[B_RIGHT]) * unitCount);

		int powers[2] = {1,1};
		if(time1[T1] == 100) // every tenth second measure velocity
		{
			powers[0] = 1;
			powers[1] = 1;
			nextClick[0] = nMotorEncoder[B_LEFT];
			nextClick[1] = nMotorEncoder[B_RIGHT];
			vels[0] = abs(nextClick[0] - prevClick[0]);
			prevClick[0] = nextClick[0];
			vels[1] = abs(nextClick[1] - prevClick[1]);
			prevClick[1] = nextClick[1];
			float velMod = 0.9;
			time1[T1] = 0; // reset the timer
			if(vels[0] > vels[1])
			{
				SensorValue[redLight] = 1;
				SensorValue[greenLight] = 0;
				powers[0] = velMod;
			}
			else if(vels[1] > vels[0])
			{
				SensorValue[redLight] = 0;
				SensorValue[greenLight] = 1;
				powers[1] = velMod;
			}
			else
			{
				SensorValue[redLight] = 0;
				SensorValue[greenLight] = 0;
			}
		}

		// proportional edit to power
		driveTrain(-sign * 50 * powers[0], sign * 50 * powers[1]);
	}
}*/
