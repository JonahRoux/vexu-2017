#pragma config(Sensor, dgtl5,  Stopper,        sensorDigitalIn)
#pragma config(Motor,  port1,           F_LEFT,        tmotorVex393_HBridge, openLoop, driveLeft, encoderPort, None)
#pragma config(Motor,  port2,           B_LEFT,        tmotorVex393_MC29, openLoop, driveLeft)
#pragma config(Motor,  port9,           F_RIGHT,       tmotorVex393_MC29, openLoop, reversed, driveRight)
#pragma config(Motor,  port10,          B_RIGHT,       tmotorVex393_HBridge, openLoop, reversed, driveRight)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

//Define useful constants
const int turn = 393; // defines encoder counts for one turn of high speed motor
const signed char halfPower = 63; // defines half speed for any motor
const signed char threshold = 15; // defines the controller deadzone threshold for filtering

void DriveTrain(signed char right, signed char left)
{
	// apply power to the motors
	motor[F_LEFT] = left;
	motor[B_LEFT] = left;
	motor[F_RIGHT] = right;
	motor[B_RIGHT] = right;
}

signed char filterJoystickAxis(signed char axis)
{
	// filter controller inputs, scale inputs for motor output
	if (abs(axis) <= threshold)
	{
		return 0;
	}
	else
	{
		return axis;
	}
}

void arcadeDrive()
{
	// grab controller inputs
	signed char forward_vector = vexRT[Ch2];
	signed char rotational_vector = vexRT[Ch4];
	// filter the joystick inputs
	forward_vector = filterJoystickAxis(forward_vector);
	rotational_vector = filterJoystickAxis(rotational_vector);

	// drive the robot according to controller inputs
	DriveTrain(forward_vector - rotational_vector, forward_vector + rotational_vector);
}

void autoDrive()
{
	resetMotorEncoder(F_LEFT);
	resetMotorEncoder(F_RIGHT);
	if(vexRT[Btn5U])
	{
		// drive forward
		while(nMotorEncoder[F_RIGHT] < (4 * turn) && nMotorEncoder[F_LEFT] < (4 * turn))
		{
			DriveTrain(halfPower, halfPower);
			/*motor[F_LEFT] = halfPower;
			motor[B_LEFT] = halfPower;
			motor[F_RIGHT] = halfPower;
			motor[B_RIGHT] = halfPower;*/
			delay(2000);
		}
	}
	else if(vexRT[Btn5D])
	{
		// drive backward
		while(nMotorEncoder[F_RIGHT] > (-4 * turn) && nMotorEncoder[F_LEFT] > (-4 * turn))
		{
			DriveTrain(-halfPower, -halfPower);
			/*motor[F_LEFT] = -halfPower;
			motor[B_LEFT] = -halfPower;
			motor[F_RIGHT] = -halfPower;
			motor[B_RIGHT] = -halfPower;*/
			delay(2000);
		}
	}

	if(SensorValue[Stopper] == 1)
	{
		resetMotorEncoder(F_LEFT);
		resetMotorEncoder(F_RIGHT);
		while(nMotorEncoder[F_RIGHT] > (-1 * turn) && nMotorEncoder[F_LEFT] > (-1 * turn))
		{
			DriveTrain(-halfPower, -halfPower);
		}
	}
}
task main()
{
	while(true)
	{
		arcadeDrive();
		//autoDrive();
	}
}
