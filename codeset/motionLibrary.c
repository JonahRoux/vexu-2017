
/*-------------------------------------------------------------
Title: 				Vex 2017/18 Motion Library
Author(s):		Nicholas Gohman, Jonah Roux, Ryan Buendorf
Date: 				1/17/2018
Note:					The folling functions run the different subsystems
							of the cone lifting robot.
Dependencies:	None
----------------------------------------------------------------*/

// global constants declaration
#define CLAW_POWER 35 					// power for the motor operating the claw
#define goalPower 50						// power for the motor operating the goal

#define LIFT_CONVERSION 100			// converting constant for "levels" to clicks
#define LIFT_MAX				1200		// max clicks for lift
#define LIFT_MIN				0				// min clicks for lift
#define LIFT_HOME				400			// home level for lift

#define CLICK_THRESHOLD 200	 		// within this threshold, allow the carriage to flip

#define click_dist			500			// forwards clicks in one foot

// sensors and limits
#define LIFT_LOW_LIMIT 		lim1
#define LIFT_HIGH_LIMIT 	lim2
#define GOAL_LIMIT 				lim3 	// limit

// global variables declaration
bool clawClosed = false;				// what is the state of the claw ?
bool allowCarriage = true;      // determines if the carriage is allowed to move
bool allowLift = true;					// determines if the lift is allowed to move
bool allowClaw = true;					// is the claw allowed to change position?
bool lastGoal = false;					// HOME goal positon, tracks last goal operation
bool lastCarriage = true;				// Home home postion of carriage is towards cones

int coneTotal = 0;							// system's count of cones
int currentLevel = 4;						// system's default value

// sensor globals
long switcher = SensorValue[GOAL_LIMIT]; // control long for goal function

//-----------------------------------
// 				BASE FUNCTIONS
//-----------------------------------

void goalDrive(signed char power)
{
	motor[goalLeft] 	= power;
	//motor[goalRight] 	= power;
}

void carriageDrive()
{
	/* This function drives the motors on the carriage
			the carriage will not operate if the claw is
			open. */

	int sign = 1;
	int preventDeath = 0;
	if(lastCarriage)
	{
		// carriage is being pulled off the stack, so need to limit automatic
		// operation of the lift
		sign = -1;
		allowLift = false;
	}
	allowClaw = false;
	while(!allowCarriage)
	{
		delay(100);	// wait for lift to permit carriage
	}
	while(abs(nMotorEncoder(carry)) < 450)
	{
		motor[carry] = 100 * sign;
	}
	while(abs(nMotorEncoder(carry)) < 900)
	{
		motor[carry] = 50 * sign;
		allowLift = true;
	}
	while(abs(nMotorEncoder(carry)) < 1000)
	{
		if(preventDeath == 5)
			break;
		motor[carry] = -10 * sign;
		delay(100);
		preventDeath += 1;
	}
	resetMotorEncoder(carry);
	lastCarriage = !lastCarriage;

	allowLift = true;
	allowClaw = true;
	//delay(1000);
	motor[carry] = 0;
	runCarriage = false;
}

void liftDrive(signed char liftLEFT, signed char liftRIGHT)	// NOT TESTED
{
	/* This function drives the motors on the 4 bar linkage
			using two arguments for the separate sides */
	//motor[topLeft] = liftLEFT;
	if(allowLift)
	{
		motor[bottomLeft] = liftLEFT;
		//motor[topRight] = liftRIGHT;
		motor[bottomRight] = liftRIGHT;
	}
}

void wheelDrive(signed char left, signed char right)
{
	/* This function drives the motors that power the
			drivetrain */
	motor[B_LEFT] = left;
	motor[F_LEFT] = left;
	motor[B_RIGHT] = right;
	motor[F_RIGHT] = right;
}

void runClaw(bool close) 					// NOT TESTED
{
	/* This function operates the claw, close command closes
			the claw. */
	signed char power = CLAW_POWER;
	if(!close)
		power = -power;
	motor[clawDrive] = power;
	delay(500);
	if(!close)
		motor[clawDrive] = 0;
		forceClaw = false;
}

void runGoal(bool toUp)
{
	int preventDeath = 0;
	bool stopPrevent = false;
	if(toUp)
	{
		while(switcher == 0 && !stopPrevent) // switch is bounced
		{
			switcher = SensorValue[GOAL_LIMIT];
			goalDrive(goalPower);
			preventDeath += 1;
			delay(500);
			if(preventDeath == 5)	// stop infinite error
				break;
		}
		stopPrevent = true;
		while(switcher == 1) // switch is debounced
		{
			// while not up
			switcher = SensorValue[GOAL_LIMIT];
			goalDrive(goalPower);
		}
		lastGoal = true;
		goalDrive(0);
	}
	else if(!toUp)
	{
		while(switcher == 0 && !stopPrevent) // switch is bounced
		{
			switcher = SensorValue[GOAL_LIMIT];
			goalDrive(-goalPower);
			preventDeath += 1;
			delay(500);
			if(preventDeath == 5) // stop infinite error
				break;
		}
		stopPrevent = true;
		while(switcher == 1) // switch is debounced
		{
			// while not down
			switcher = SensorValue[GOAL_LIMIT];
			goalDrive(-goalPower);
		}
		lastGoal = false;
		goalDrive(0);
	}
}

//-----------------------------------
// 				ASSIST FUNCTIONS
//-----------------------------------
void turn(int dir)
{
	/* using a degree value for argument, positive indicates a clockwise turn,
	negative denotes a counterclockwise turn. Requires analog gyro */
	int degrees10 = dir * 10;
	int originalPos = abs(SensorValue[gyroIn]);
	pidRequestedValue[2] = originalPos + degrees10;
}

void goStraight(int dist)
{
	int sign = 1;
	if(dist < 0)
		sign = -1;
	float clickTotal = abs(dist) * click_dist;
	float traveled = 0;
	forMod = 100;
	while(abs(traveled) < (abs(clickTotal) - 30))
	{
		// do nothing
		delay(10);
	}
	forMod = 0;
}

void goHome()
{
	// this function brings the robot to its home position
	forceClaw = true;
	if(!lastCarriage)
	{
		// bring the carriage forward
		if(!runCarriage)
			runCarriage = true;
	}
	if(lastGoal)
	{
		runGoal(true);
	}
	pidRequestedValue[0] = LIFT_HOME;	// bring the lift to its natural position
	pidRequestedValue[1] = LIFT_HOME;
	currentLevel = 4;
}

void liftAuto(int level)	// REALLY NOT TESTED
{
	/* This function is used by both autonomous and manual to lift
			the lift to the correct place to stack, this function needs
			a limit switch at the extremes of lift motion */
	// find out where the lift is going
	if((level - currentLevel) > 0)
	{
		// lift going up, halt carriage operation
		allowCarriage = false;
	}
	pidRequestedValue[0] = level * LIFT_CONVERSION;
	pidRequestedValue[1] = level * LIFT_CONVERSION;

	if(!lim1)
	{
		resetMotorEncoder(bottomLeft);
		resetMotorEncoder(bottomRight);
	}
	if(!lim2)
	{
		nMotorEncoder[bottomLeft] = 	LIFT_MAX;
		nMotorEncoder[bottomRight] = 	LIFT_MAX;
	}

	currentLevel = level;
}

void collection()
{
	// starting from home postion
	int preventDeath = 0;
	goHome();
	while(abs(nMotorEncoder(bottomLeft) - LIFT_HOME) > 100)
	{
		// do nothing, wait til correct position
		delay(10);
	}
	liftAuto(0); // bring the lift to hard stop limit
	// force the lift down, if hardstop or limit pull out
	delay(500);
	while(lim1)
	{
		if(preventDeath == 20)
		{
			resetMotorEncoder(bottomLeft);
			resetMotorEncoder(bottomRight);
			break;
		}
		pidRequestedValue[0] = pidRequestedValue[0] - 10;
		pidRequestedValue[1] = pidRequestedValue[1] - 10;

	  wait1Msec(50);
	  preventDeath += 1;

	}
	// grab the cone
	runClaw(true);
	liftAuto(coneTotal);
	runCarriage = true;
	while(!allowClaw)
	{
		// do nothing
		delay(10);
	}
	runClaw(false);
	coneTotal += 1;
	goHome();
	while(abs(nMotorEncoder(bottomLeft) - LIFT_HOME) > 100)
	{
		// do nothing, wait til correct position
		delay(10);
	}
}
