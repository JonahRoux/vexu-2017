/*-------------------------------------------------
Function that applies power to the motors
in the drive train. Inputs are right side power and
left side power.
--------------------------------------------------*/

void wheelDrive(signed char left, signed char right)
{
	motor[B_LEFT] = left;
	motor[F_LEFT] = left;
	motor[B_RIGHT] = right;
	motor[F_RIGHT] = right;
}

/*-------------------------------------------------
Function that applies power to the motors
in the lift. Inputs are right side power and
left side power.
--------------------------------------------------*/

void liftDrive(float liftPow)
{
	motor[TOP_RIGHT] =  liftPow;
	motor[BOTTOM_RIGHT] = liftPow;
	motor[TOP_LEFT] = liftPow;
	motor[BOTTOM_LEFT] = liftPow;
}

void pneumaToggle(int set)
{
	SensorValue[sol1] = set;
	SensorValue[sol2] = set;
}

void carriageDrive()
{
	if(vexRT[cntrlCARR_UP])
	{
		motor[CARRIAGE] = 70;
	}
	else if(vexRT[cntrlCARR_DOWN])
	{
		motor[CARRIAGE] = -70;
	}
	else
	{
		motor[CARRIAGE] = 0;
	}
}

void grabberDrive(signed char grabPow)
{
	motor[GRABBER] = grabPow;
}


//---------------------------------------------
// Logical Functions
//---------------------------------------

signed char filterJoystickAxis(signed char axis, float multConst)
{
	// filter controller inputs, scale inputs for motor output
	if (abs(axis) <= threshold)
	{
		return 0;
	}
	else
	{ // mathematical function to scale output
		return (pow(axis, 3)/16129) * multConst;
	}
}

void arcadeDrive()
{
	float rot_const = 0.85;
	float str_const = 0.9;
	long motValues[2] = {0, 0};
	// grab controller inputs
	signed char forward_vector    = vexRT[cntrlFOR];
	signed char rotational_vector = -vexRT[cntrlROT];
	// filter the joystick inputs
	forward_vector = filterJoystickAxis(forward_vector, str_const);
	rotational_vector = filterJoystickAxis(rotational_vector, rot_const);

	// fix write values so they don't overwrite
	motValues[0] = forward_vector - rotational_vector;
	motValues[1] = forward_vector + rotational_vector;
	for(int i = 0; i < 2; i++)
	{
		if(motValues[i] > 127)
		{
			motValues[i] = 120;
		}
		else if(motValues[i] < -127)
		{
			motValues[i] = -120;
		}
	}
	// drive the robot according to controller inputs
	wheelDrive(motValues[0], motValues[1]);
}
